import random
def menu():
    print("Witaj w grze odgadywania słowa!")
    print("1 ---> Wylosuj słowo")
    print("2 ---> Modyfikuj pule słów")
    print("3 ---> Zakończ")
    x=input("Wybierz numer pozycji:")
    if x=="1":
        gra()
    elif x=="2":
        admin()
    elif x=="3":
        exit()
    else:
        print("Błąd, spróbuj ponownie")
        input("Kliknij dowolny klawisz aby kontynuować...")
        menu()
def wylosuj():
    try:
        with open("slowa.txt", "r", encoding="utf=8") as plik:
            slowo=plik.read().splitlines()
        return random.choice(slowo)
    except FileNotFoundError:
        print("Brak odpowiednich plików.")
        return []
    except Exception:
        print('Wystąpił błąd.')
        return []
def admin():
    print("Witaj w trybie modyfikacji!")
    print("1 ---> Dodaj nowe słowo do puli")
    print("2 ---> Usuń istniejące słowo w puli")
    print("3 ---> Zmodyfikuj już istniejące słowo w puli")
    print("4 ---> Powrót do menu")
    print("5 ---> Zakończ")
    h=input("Wybierz numer pozycji:")
    if h=="1":
        dod()
    elif h=="2":
        usun()
    elif h=="3":
        mod()
    elif h=="4":
        menu()
    elif h=="5":
        exit()
    else:
        print("Błąd, spróbuj ponownie")
        input("Kliknij dowolny klawisz aby kontynuować...")
        admin()
def dod():
    try:
        with open("slowa.txt", "a",encoding="utf=8") as plik:
            slowo=input("Podaj słowo do dodania do puli:")
            plik.write(slowo + "\n")
            print("Słowo dodane do puli")
    except Exception:
        print("Wystąpił błąd.")
    admin()
def usun():
    try:
        with open("slowa.txt", "r",encoding="utf=8") as plik:
            slowa=plik.readlines()
            slowo=input("Podaj słowo do usunięcia: ")
            modslowa=[]
            z=0
            for linia in slowa:
                if slowo in linia:
                    z+=1
                if linia.strip()!=slowo:
                    modslowa.append(linia)
        if z==1:
            with open("slowa.txt", "w",encoding="utf=8") as plik:
                plik.writelines(modslowa)
            print("Slowo zostało usunięte z bazy")
        else:
            print("Słowa nie znaleziono w bazie")
    except Exception:
        print("Wystąpił błąd")
    admin()
def mod():
    try:
        with open("slowa.txt", "r",encoding="utf=8") as plik:
            slowa=plik.readlines()
            slowo=input("Podaj słowo do modyfikacji: ")
            modslowo=input("Podaj nowe słowo: ")
            modslowa=[]
            v=0
            for linia in slowa:
                if linia.strip()==slowo:
                    modslowa.append(modslowo + '\n')
                    v+=1
                    print("Słowo:", slowo, "zostało zmienione na",modslowo)
                else:
                    modslowa.append(linia)
        if v==1:
            with open("slowa.txt", "w",encoding="utf=8") as plik:
                plik.writelines(modslowa)
        else:
            print("Nie znaleziono w bazie słowa:", slowo,"do modyfikacji")
    except Exception:
        print("Wystąpił błąd")
    admin()
def wskazowka():
    print("1 ---> Wyświetl litery, które już probowałeś")
    print("2 ---> Odsłoń losową litere, po rozwiązaniu krótkiego zadania matematycznego")
    print("3 ---> Zwiększ ilość możliwych błędnych prób odgadnięcia(+3)")
    print("4 ---> Poddaj się")
def mat():
    w=random.randint(1, 99)
    s=random.randint(1, 99)
    d=random.choice(["+", "-", "*"])
    pytanie=f"{w} {d} {s}"
    wynik=eval(pytanie)
    return wynik, pytanie
def ods(ukryte,slowo,dobrelitery):
    indeksy=[]
    for i, litera in enumerate(ukryte):
        if litera=="_":
            indeksy.append(i)
    if indeksy!=[]:
        indeks=random.choice(indeksy)
        lista=list(ukryte)
        lista[indeks]=slowo[indeks]
        y=slowo[indeks]
        dobrelitery.append(y)
        ukryte="".join(lista)
        print("Odsłonięta litera to:",y)
    return ukryte, dobrelitery
def ukryj(slowo,dobrelitery):
    ukryte=""
    for litera in slowo:
        if litera in dobrelitery:
            ukryte += litera
        else:
            ukryte += "_"
    return ukryte
def gra():
    maxproba=9
    slowo=wylosuj()
    uzytelitery=[]
    dobrelitery=[]
    proba=0
    ukryte="_"
    if slowo==[]:
        input("Kliknij dowolny klawisz aby kontynuować...")
        menu()
    ukryte=ukryj(slowo, dobrelitery)
    print("Ukryte słowo to --->",ukryte)
    while True:
        while proba<=maxproba:
            litera=input("Podaj literę:").lower()
            if litera=="?":
                wskazowka()
                a=input("Wybierz numer pozycji:")
                if a=="1":
                    print("Litery, które już spróbowałeś to:")
                    print(uzytelitery)
                    print("Ukryte słowo to --->", ukryte)
                elif a=="2":
                    wynik,pytanie=mat()
                    print("Wybrałeś rozwiązanie prostego zadania!")
                    print("Oblicz poniższe równanie:",pytanie)
                    c=int(input("Odpowiedź:"))
                    if wynik==c:
                        print("Gratulacje!! Poprawna odpowiedź")
                        print("Jako nagroda zostanie odsłonięta jedna litra")
                        ods(ukryte,slowo,dobrelitery)
                        ukryte=ukryj(slowo, dobrelitery)
                        print("Ukryte słowo to --->", ukryte)
                        if "_" not in ukryte:
                            print("Odgadłeś całe słowo. Gratulacje!!! Udało ci się po", proba,"błędnych próbach.")
                            p=input("Kliknij dowolny klawisz aby kontynuować...")
                            menu()
                    else:
                        print("Źle! Nie udało ci się odpowiedzieć poprawnie")
                        ukryte=ukryj(slowo, dobrelitery)
                        print("Ukryte słowo to --->", ukryte)
                elif a=="3":
                    maxproba+=3
                    print("Dodano dodatkowe próby! Aktualna liczba maksymalnych prób to:",maxproba,"\nAktualne błędne próby:",proba)
                    print("Ukryte słowo to --->", ukryte)
                elif a=="4":
                    print("Przegrałeś!\nPoddałeś się po:",proba,"błędnych próbach\nUkryte słowo to:", slowo)
                    input("Kliknij dowolny klawisz aby kontynuować...")
                    menu()
                else:
                    print("Błąd! Powrót do gry")
                    input("Kliknij dowolny klawisz aby kontynuować...")
            if litera.isalpha() and len(litera)==1:
                if litera in dobrelitery:
                    proba+=1
                    b=maxproba-proba
                    if b>0:
                        print("Ta litera już została przez ciebie odgadnięta!\nUwaga!!!Powtórzone litery też liczą się jako błędy!\nPozostało",b,"błędnych prób")
                    elif b==0:
                        print("Nie została ci już żadna błędna próba!!! Jeśli odpowiesz źle - przegrasz!")
                    if proba>=3:
                        print("Jeśli potrzebujesz pomocy skorzystaj z wskazówek!(Kliknij ? aby użyć wskazówek)")
                elif litera not in slowo:
                    proba+=1
                    b=maxproba-proba
                    if b>0:
                        print("Nieprawidłowa litera!\nPozostało",b,"błędnych prób")
                    elif b==0:
                        print("Nie została ci już żadna błędna próba!!! Jeśli odpowiesz źle - przegrasz!")
                    if proba>=3:
                        print("Jeśli potrzebujesz pomocy skorzystaj z wskazówek!(Kliknij ? aby użyć wskazówek)")
                else:
                    dobrelitery.append(litera)
                    print("Dobra robota! Zgadłeś litere!")
                if litera not in uzytelitery:
                    uzytelitery.append(litera)
                ukryte=ukryj(slowo, dobrelitery)
                print("Ukryte słowo to --->",ukryte)
            if "_" not in ukryte:
                print("Odgadłeś całe słowo. Gratulacje!!! Udało ci się po",proba,"błędnych próbach.")
                p=input("Kliknij dowolny klawisz aby kontynuować...")
                menu()
        else:
            print("Przegrałeś!\nZa duża ilość błędnych liter lub powtórzeń!\nUkryte słowo to:", slowo)
            p=input("Kliknij dowolny klawisz aby kontynuować...")
            menu()

menu()